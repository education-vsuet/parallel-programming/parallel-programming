﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Files
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            button4.Enabled = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //создание контекстного меню
            System.Windows.Forms.ContextMenu contextMenu1;
            contextMenu1 = new System.
            Windows.Forms.ContextMenu();
            System.Windows.Forms.MenuItem menuItem1;
            menuItem1 = new System.Windows.Forms.MenuItem();
            System.Windows.Forms.MenuItem menuItem2;
            menuItem2 = new System.Windows.Forms.MenuItem();
            System.Windows.Forms.MenuItem menuItem3;
            menuItem3 = new System.Windows.Forms.MenuItem();
            contextMenu1.MenuItems.AddRange(new System.
            Windows.Forms.MenuItem[] { menuItem1, menuItem2, menuItem3 });
            menuItem1.Index = 0;
            menuItem1.Text = "Открыть";
            menuItem2.Index = 1;
            menuItem2.Text = "Сохранить";
            menuItem3.Index = 2;
            menuItem3.Text = "Сохранить как";
            richTextBox1.ContextMenu = contextMenu1;
            menuItem1.Click += new System.
            EventHandler(this.menuItem1_Click);
            menuItem2.Click += new System.
            EventHandler(this.menuItem2_Click);
            menuItem3.Click += new System.
            EventHandler(this.menuItem3_Click);
        }

        string MyFName = "";
        private void menuItem1_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Текстовые Files (*.rtf; *.txt; *.dat) | *.rtf; *.txt; *.dat";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                MyFName = openFileDialog1.FileName;
                richTextBox1.LoadFile(MyFName);
            }
        }

        private void menuItem2_Click(object sender, EventArgs e)
        {
            if (MyFName != "")
            {
                richTextBox1.SaveFile(MyFName);
            }
            else
            {
                saveFileDialog1.Filter = "Текстовые Files (*.rtf; *.txt; *.dat) | *.rtf; *.txt; *.dat";
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    MyFName = saveFileDialog1.FileName;
                    richTextBox1.SaveFile(MyFName);
                }
            }
        }

        private void menuItem3_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Текстовые Files (*.rtf; *.txt; *.dat) | *.rtf; *.txt; *.dat";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                MyFName = saveFileDialog1.FileName;
                richTextBox1.SaveFile(MyFName);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            richTextBox1.Clear();
            button2.Enabled = true;
            button3.Enabled = true;
            button4.Enabled = true;
        }

        int result1, result2;
        private void button2_Click(object sender, EventArgs e)
        {
            int LenText;
            textBox1.Text += "Поиск первого слова" + Environment.NewLine;
            String FWord = textBox2.Text.ToString();
            LenText = richTextBox1.Text.Length;
            result1 = FindWord(FWord, LenText);
            if (result1 != -1)
            {
                textBox1.Text += "Позиция первого слова: " + (result1 + 1) + Environment.NewLine + Environment.NewLine;
                richTextBox1.SelectionStart = result1;
                richTextBox1.SelectionLength = FWord.Length;
                richTextBox1.SelectionBackColor = Color.Red;
                button2.Enabled = false;
                if (button3.Enabled == false)
                {
                    button4.Enabled = true;
                }
            }
            else
            {
                textBox1.Text += "Слово не найдено " + Environment.NewLine + Environment.NewLine;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int LenText;
            textBox1.Text += "Поиск второго слова" + Environment.NewLine;
            String FWord = textBox3.Text.ToString();
            LenText = richTextBox1.Text.Length;
            result2 = FindWord(FWord, LenText);
            if (result2 != -1)
            {
                textBox1.Text += "Позиция второго слова: " + (result2+1) + Environment.NewLine + Environment.NewLine;
                richTextBox1.SelectionStart = result2;
                richTextBox1.SelectionLength = FWord.Length;
                richTextBox1.SelectionBackColor = Color.Green;
                button3.Enabled = false;
                if (button2.Enabled == false)
                {
                    button4.Enabled = true;
                }
            }
            else
            {
            textBox1.Text += "Слово не найдено " + Environment.NewLine + Environment.NewLine;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (result1 < result2)
            {
                richTextBox1.Select(result2, textBox3.Text.Length);
                richTextBox1.SelectedText = textBox2.Text.ToString();
                richTextBox1.Select(result1, textBox2.Text.Length);
                richTextBox1.SelectedText = textBox3.Text.ToString();
                textBox1.Text += "Произошла замена слов";
                button4.Enabled = false;
            }
            else
            {
                richTextBox1.Select(result1, textBox2.Text.Length);
                richTextBox1.SelectedText = textBox3.Text.ToString();
                richTextBox1.Select(result2, textBox3.Text.Length);
                richTextBox1.SelectedText = textBox2.Text.ToString();
                textBox1.Text += "Произошла замена слов";
                button4.Enabled = false;
            }
        }

        int FindWord(String FWord, int n)
        {
        int LenWord;
        String ComparText;
        LenWord = FWord.Length;
        for (int i = 0; i <= n - LenWord; i++)
        {
            ComparText = richTextBox1.Text.Substring(i, LenWord);
            if (ComparText == FWord)
            {
                return i;
            }
        }
        return -1;
        }

        List<int> FindAllWords(String FWord, int n)
        {
            int LenWord;
            List<int> Result = new List<int>();
            String ComparText;
            LenWord = FWord.Length;
            for (int i = 0; i <= n - LenWord; i++)
            {
                ComparText = richTextBox1.Text.Substring(i, LenWord);
                if (ComparText == FWord)
                {
                    Result.Add(i);
                }
            }
            return Result;
        }

        List<Coordinates> GetAllWordCoordinates(String Text, String FWord)
        {
            int LenWord;
            int CurrentSentence = 1;
            List<Coordinates> Result = new List<Coordinates>();
            String ComparText;
            LenWord = FWord.Length;
            for (int i = 0; i <= Text.Length - LenWord; i++)
            {
                ComparText = Text.Substring(i, LenWord);
                if (ComparText == FWord)
                {
                    Result.Add(new Coordinates(/*номер предложения*/ CurrentSentence, /*номер первого символа слова*/ i));
                } else if (IsSentenceEndSymbol(Text.Substring(i, 1)))
                {
                    CurrentSentence += 1;
                }
            }
            return Result;
        }

        const String END_SENTENCE = ".?!";
        public bool IsSentenceEndSymbol(String Symbol)
        {
            return END_SENTENCE.IndexOf(Symbol) >= 0;
        }

        public void SelectSentence(int Sentence, String Text)
        {
            int SelectionStart = 1;
            int SelectionEnd = -1;
            int SentenceIndex = 1;
            String ComparText;
            for (int i = 0; i < Text.Length; i++)
            {
                ComparText = Text.Substring(i, 1);
                if (IsSentenceEndSymbol(Text.Substring(i, 1)))
                {
                    if (SentenceIndex == Sentence)
                    {
                        SelectionEnd = i;
                        richTextBox1.SelectionStart = SelectionStart - 1;
                        richTextBox1.SelectionLength = SelectionEnd - SelectionStart + 1;
                        richTextBox1.SelectionBackColor = Color.Aqua;
                        //richTextBox1.Select();
                        return;
                    }
                    else
                    {
                        SentenceIndex += 1;
                        SelectionStart = i + 1;
                    }
                }
            }
        }

        public int FindNextEndSymbol(string Text, int AfterSentence)
        {
            int SentenceIndex = 1;
            for (int i = 0; i < Text.Length; i++)
            {
                if (IsSentenceEndSymbol(Text.Substring(i, 1)))
                {
                    if (SentenceIndex == AfterSentence)
                    {
                        return i;
                    }
                    else
                    {
                        SentenceIndex += 1;
                    }
                }
            }
            return -1;
        }

        public struct Coordinates
        {
            public int SentenceNumber;
            public int WordPosition;

            public Coordinates(int _SentenceStart, int _WordPosition)
            {
                SentenceNumber = _SentenceStart;
                WordPosition = _WordPosition;
            }
        }

        public List<String> GetSentences(String Text, String Word)
        {
            var Sentences = Text.Split(new[] { ". " }, StringSplitOptions.RemoveEmptyEntries);
            var Matches = from Sentence in Sentences
                          where Sentence.ToLower().Contains(Word.ToLower())
                          select Sentence;
            return Matches.ToList();
        }


        public struct WordsIteration
        {
            public List<Coordinates> WordPositions;
            public int CurrentIndex;
            public String Word;
        }
        WordsIteration Words;
        private void button5_Click(object sender, EventArgs e)
        {
            textBox4.Visible = true;
            label1.Visible = true;
            label1.Text = "Позиция слова " + textBox2.Text.ToString() + " в каждом предложении";
            int LenText;
            textBox1.Text += "Поиск слова в предложениях" + Environment.NewLine;
            String FWord = textBox2.Text.ToString();
            LenText = richTextBox1.Text.Length;
            Words = new WordsIteration();
            Words.WordPositions = GetAllWordCoordinates(richTextBox1.Text, FWord);
            foreach (var i in Words.WordPositions)
            {
                textBox4.Text += "Найдена позиция слова " + FWord + ": " + i.WordPosition + " в предложении " + i.SentenceNumber + Environment.NewLine;
            }
            button6.Visible = true;
            Words.CurrentIndex = -1;
        }


        private void button6_Click(object sender, EventArgs e)
        {
            if (Words.WordPositions.Count > 0)
            {
                richTextBox1.SelectionStart = 0;
                richTextBox1.SelectionLength = richTextBox1.Text.Length;
                richTextBox1.SelectionBackColor = Color.White;
                Words.CurrentIndex = (Words.CurrentIndex + 1) % Words.WordPositions.Count;
                SelectSentence(Words.WordPositions[Words.CurrentIndex].SentenceNumber, richTextBox1.Text);
                //richTextBox1.SelectionStart = Words.WordPositions[Words.CurrentIndex].WordPosition;
                //richTextBox1.SelectionLength = Words.Word.Length;
                //richTextBox1.SelectionBackColor = Color.Aqua;
                //richTextBox1.Select();
            }
            else
            {
                Words.CurrentIndex = -1;
            }
        }
    }
}
