﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Structures
{
    struct Employee
    {
        public string FIO;//ФИО
        public string Post;// Должность
        public string Date_of_Birth;// Дата рождения
        public string Degree; //Ученая степень
        public int Experience;//Стаж работы
        public Employee(string f, string p, string d, string deg, int e)//конструктор
        {
            FIO = f;
            Post = p;
            Date_of_Birth = d;
            Degree = deg;
            Experience = e;
        }
    }

    struct Street
    {
        public string Name;//Наименование улицы
        public Int32 Length;//Длина
        public string History;//История
        public string Place; //Район
        public Street(string n, int l, string h, string p)//конструктор
        {
            Name = n;
            Length = l;
            History = h;
            Place = p;
        }
    }

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox1.Items.Add("Преподаватель");
            comboBox1.Items.Add("Ст. преподаватель");
            comboBox1.Items.Add("Доцент");
            comboBox1.Items.Add("Профессор");
            comboBox2.Items.AddRange(new object[] {
                "Без уч. степени",
                "Кандидат наук",
                "Доктор наук"
                });
            dataGridView1.RowHeadersVisible = false;
            dataGridView1.ColumnCount = 5;
            dataGridView1.Columns[0].HeaderText = "ФИО";
            dataGridView1.Columns[1].HeaderText = "Должность";
            dataGridView1.Columns[2].HeaderText = "Дата рождения";
            dataGridView1.Columns[3].HeaderText = "Ученая степень";
            dataGridView1.Columns[4].HeaderText = "Стаж";
            dataGridView2.RowHeadersVisible = false;
            dataGridView2.ColumnCount = 5;
            dataGridView2.Columns[0].HeaderText = "ФИО";
            dataGridView2.Columns[1].HeaderText = "Должность";
            dataGridView2.Columns[2].HeaderText = "Дата рождения";
            dataGridView2.Columns[3].HeaderText = "Ученая степень";
            dataGridView2.Columns[4].HeaderText = "Стаж";
            dataGridView2.RowHeadersVisible = false;
            dataGridView4.RowHeadersVisible = false;
            dataGridView4.ColumnCount = 5;
            dataGridView4.Columns[0].HeaderText = "Наименование";
            dataGridView4.Columns[1].HeaderText = "Длина";
            dataGridView4.Columns[2].HeaderText = "История";
            dataGridView4.Columns[3].HeaderText = "Район";
            dataGridView3.RowHeadersVisible = false;
            dataGridView3.ColumnCount = 5;
            dataGridView3.Columns[0].HeaderText = "Наименование";
            dataGridView3.Columns[1].HeaderText = "Длина";
            dataGridView3.Columns[2].HeaderText = "История";
            dataGridView3.Columns[3].HeaderText = "Район";
        }

        Employee[] worker = new Employee[10];
        int count = 0;
        private void button1_Click(object sender, EventArgs e)
        {
            worker[count].FIO = textBox1.Text;
            worker[count].Post = comboBox1.Text;
            worker[count].Date_of_Birth =
            dateTimePicker1.Value.ToString("dd.MM.yyyy");
            worker[count].Degree = comboBox2.Text;
            worker[count].Experience =
            Convert.ToInt32(textBox2.Text);
            dataGridView1.Rows.Add(worker[count].FIO,
            worker[count].Post, worker[count].Date_of_Birth,
            worker[count].Degree, worker[count].Experience.ToString());
            count++;
        }

        List<Street> streets = new List<Street>();
        int streetsCount = 0;
        private void button4_Click(object sender, EventArgs e)
        {
            streets.Add(new Street(textBox6.Text, Convert.ToInt32(numericUpDown1.Value), textBox5.Text, textBox7.Text));
            dataGridView4.Rows.Add(textBox6.Text,
                                   Convert.ToInt32(numericUpDown1.Value),
                                   textBox5.Text,
                                   textBox7.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked == true)
            {
                dataGridView2.Rows.Clear();
                int select1 = Convert.ToInt32(textBox3.Text);
                foreach (Employee wSel in worker)
                {
                    if (wSel.Experience >= select1)
                        dataGridView2.Rows.Add(wSel.FIO,
                        wSel.Post, wSel.Date_of_Birth, wSel.Degree,
                        wSel.Experience.ToString());
                }
            }
            if (radioButton2.Checked == true)
            {
                dataGridView2.Rows.Clear();
                string select2 = textBox3.Text;
                foreach (Employee wSel in worker)
                {
                    if (wSel.Post == select2)
                        dataGridView2.Rows.Add(wSel.FIO,
                        wSel.Post, wSel.Date_of_Birth, wSel.Degree, wSel.Experience.
                        ToString());
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (radioButton4.Checked == true)
            {
                dataGridView3.Rows.Clear();
                List<Street> tempRows = streets.OrderBy(o=>o.Place).ToList();
                foreach (Street wSel in tempRows)
                    if (wSel.Name != "")
                    {
                        dataGridView3.Rows.Add(wSel.Name,
                                               wSel.Length,
                                               wSel.History,
                                               wSel.Place);
                    }
            }
        }

        private void addDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            button1_Click(sender, e);
        }

        private void findItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            button2_Click(sender, e);
        }

        private void QuitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}